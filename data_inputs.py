# -*- coding: utf-8 -*-

import librosa
import os
import glob
import numpy as np
import json
import random

def main():
 	for_save_list = input_musics()
 	
 	with open('song_features.json','w')as f:
 		json.dump(for_save_list, f, sort_keys=True, indent=4)
 	

def input_musics():
	filenames = glob.glob(os.path.join('/Users/kosukefukui/Music/iTunes/iTunes Media/Music/*/*/*.mp3'))

	#実験用サンプリング
	files = random.sample(filenames,10)


	"""
	課題
	(599frames,128bins）の入力データの生成
	"""
	mel_spectrum_list = []

	print "------lodaging data------"
	
	i = 0

	for mp3_song in files:
		y, sr = librosa.load(mp3_song, duration = 13.9)
		for_append = librosa.feature.melspectrogram(y=y, sr=sr, n_mels =128)
		for_append = for_append.T

		i = i + 1
		print i

		print "行列のサイズ"
		print for_append.shape

		if for_append.shape[0] == 599:
			for_append = for_append.tolist()
			mel_spectrum_list.append(for_append)

	return mel_spectrum_list

main()
