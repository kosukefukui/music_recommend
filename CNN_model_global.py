# -*- coding: utf-8 -*-
import tensorflow as tf
import time
import json
import numpy as np
import matplotlib.pyplot as plt
import random
import multiprocessing as mp

# 定義用のflagsを作成
flags = tf.app.flags
 
# 値取得用のFLAGSを作成
FLAGS = flags.FLAGS

flags.DEFINE_string('train_dir', '/tmp/data', 'Directory to put the training data.')
flags.DEFINE_integer('max_steps', 3, 'Number of steps to run trainer.')
flags.DEFINE_integer('batch_size', 10, 'Batch size'
					 'Must divide evenly into the dataset sizes.')
flags.DEFINE_float('learning_rate', 1e-4, 'Initial learning rate.')

NUM_CLASSES = 5
IMAGE_SIZE = 599
IMAGE_PIXELS = IMAGE_SIZE*1*128

def inference(images_placeholder, keep_prob):

	# 重みを標準偏差0.1の正規分布で初期化
	def weight_variable(shape):
		initial = tf.truncated_normal(shape, stddev=0.1)
		return tf.Variable(initial)

	# バイアスを標準偏差0.1の正規分布で初期化
	def bias_variable(shape):
		initial = tf.constant(0.1, shape=shape)
		return tf.Variable(initial)

	# 第一層畳み込み層の作成
	def conv2d(x, W):
		return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

	# X2プーリング層の作成
	def max_pool_2x128(x):
		return tf.nn.max_pool(x, ksize=[1, 2, 1, 1],strides=[1, 2, 1, 1], padding='VALID')
	# X4プーリング層の作成
	def max_pool_4x128(x):
		return tf.nn.max_pool(x, ksize=[1, 4, 1, 1],strides=[1, 4, 1, 1], padding='VALID')

	#第一層畳み込み
	with tf.name_scope('conv1') as scope:
		W_conv1 = weight_variable([4, 1, 128, 256])
		b_conv1 = bias_variable([256])

		x_image = tf.reshape(images_placeholder, [-1,599,1,128])

		print "image変形後のshape"
		print tf.Tensor.get_shape(x_image)
		print "conv1の形"
		print tf.Tensor.get_shape(conv2d(x_image, W_conv1))
		
		h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)

	#第一層プーリング X4
	with tf.name_scope('pool1') as scope:
		h_pool1 = max_pool_4x128(h_conv1)
		print "h_pool1の形"
		print tf.Tensor.get_shape(h_pool1)

	#第二層畳み込み
	with tf.name_scope('conv2') as scope:
		W_conv2 = weight_variable([4, 1, 256, 256])
		b_conv2 = bias_variable([256])
		h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)

	#第二層プーリング X2
	with tf.name_scope('pool2') as scope:
		h_pool2 = max_pool_2x128(h_conv2)
		print "h_pool2の形"
		print tf.Tensor.get_shape(h_pool2)

	#第三層畳み込み
	with tf.name_scope('conv3') as scope:
		W_conv3 = weight_variable([4, 1, 256, 512])
		b_conv3 = bias_variable([512])
		h_conv3 = tf.nn.relu(conv2d(h_pool2, W_conv3) + b_conv3)

	#第三層プーリング X4
	with tf.name_scope('pool3') as scope:
		h_pool3 = max_pool_2x128(h_conv3)
		print "h_pool3の形"
		print tf.Tensor.get_shape(h_pool3)

	
	#Global Temporal Pooling
	with tf.name_scope('global_pool') as scope:
		#Mean pooling
		p_mean1 = tf.reduce_mean(h_pool3,0)
		p_mean2 = tf.reduce_mean(p_mean1,0)
		mean_pool = tf.reduce_mean(p_mean2,0)
		print tf.Tensor.get_shape(mean_pool)

		#Max pooling
		p_max1 = tf.reduce_max(h_pool3,0)
		p_max2 = tf.reduce_max(p_max1,0)
		max_pool = tf.reduce_max(p_max2,0)
		print tf.Tensor.get_shape(max_pool)

		#L2 poolong
		p_square1 = tf.square(h_pool3)
		p_square2 = tf.reduce_mean(p_square1,0)
		p_square3 = tf.reduce_mean(p_square2,0)
		l2_pool = tf.reduce_mean(p_square3,0)
		print tf.Tensor.get_shape(l2_pool)

		global_pool = tf.concat(0,[mean_pool,max_pool,l2_pool])
		global_pool = tf.reshape(global_pool,[-1, 1536])
		print "Global poolingの形"
		print tf.Tensor.get_shape(global_pool)


	#フラット化、第一結合層
	with tf.name_scope('fc1') as scope:
		W_fc1 = weight_variable([1536, 2048])
		b_fc1 = bias_variable([2048])
		h_fc1 = tf.nn.relu(tf.matmul(global_pool, W_fc1) + b_fc1)
		#ドロップ層の設定
		h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

	#第二結合層
	with tf.name_scope('fc2') as scope:
		W_fc2 = weight_variable([2048, NUM_CLASSES])
		b_fc2 = bias_variable([NUM_CLASSES])

	#出力層
	with tf.name_scope('softmax') as scope:
		y_conv=tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

	return y_conv

def loss(logits, labels):
	""" lossを計算する関数

	引数:
	  logits: ロジットのtensor, float - [batch_size, NUM_CLASSES]
	  labels: ラベルのtensor, int32 - [batch_size, NUM_CLASSES]

	返り値:
	  cross_entropy: 交差エントロピーのtensor, float

	"""

	# 交差エントロピーの計算
	cross_entropy = -tf.reduce_sum(labels*tf.log(tf.clip_by_value(logits,1e-10,1.0)))
	# TensorBoardで表示するよう指定
	tf.scalar_summary("cross_entropy", cross_entropy)
	return cross_entropy


def training(loss, learning_rate):
	""" 訓練のopを定義する関数

	引数:
	  loss: 損失のtensor, loss()の結果
	  learning_rate: 学習係数

	返り値:
	  train_step: 訓練のop

	"""

	train_step = tf.train.AdamOptimizer(learning_rate).minimize(loss)
	return train_step

def accuracy(logits, labels):
	""" 正解率(accuracy)を計算する関数

	引数: 
	  logits: inference()の結果
	  labels: ラベルのtensor, int32 - [batch_size, NUM_CLASSES]

	返り値:
	  accuracy: 正解率(float)

	"""
	correct_prediction = tf.equal(tf.argmax(logits, 1), tf.argmax(labels, 1))
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
	tf.scalar_summary("accuracy", accuracy)
	return accuracy

if __name__ == '__main__':
	
	##################
	#教師あり学習ですが、ラベルがないので適当につくります。
	##################
	# ファイルを開く
	f = open("song_features.json")
	data = json.load(f)
	data = np.array(data)

	flatten_data = []
	flatten_label = []
	
	
	for line in range(len(data)):
		for_flat = np.array(data[line])
		assert for_flat.flatten().shape == (IMAGE_PIXELS,)
		flatten_data.append(for_flat.flatten().tolist())
		
		#ラベルはとりあえず適当。1-of-K方式。
		tmp = np.zeros(NUM_CLASSES)
		tmp[int(random.randint(0,4))] = 1
		flatten_label.append(tmp)

	#一列に整形した訓練データ
	train_image = np.asarray(flatten_data, dtype=np.float32)
	train_label = np.asarray(flatten_label,dtype=np.float32)

	flatten_test_image = []
	flatten_test_label = []

	train_num = 5
	
	for line2 in range(train_num, len(data)):
		for_flat2 = np.array(data[line2])
		flatten_test_image.append(for_flat2.flatten().tolist())
		
		#ラベルはとりあえず適当。1-of-K方式。
		tmp = np.zeros(NUM_CLASSES)
		tmp[int(random.randint(0,4))] = 1
		flatten_test_label.append(tmp)

	test_image = np.asarray(flatten_test_image, dtype=np.float32)
	test_label = np.asarray(flatten_test_label, dtype=np.float32)
	
	print "599×128 = "
	print len(train_image[0])
	
	f.close()

	if 1==1:
		# 画像を入れる仮のTensor
		images_placeholder = tf.placeholder("float", shape=(None, IMAGE_PIXELS))
		# ラベルを入れる仮のTensor
		labels_placeholder = tf.placeholder("float", shape=(None, NUM_CLASSES))
		# dropout率を入れる仮のTensor
		keep_prob = tf.placeholder("float")

		# inference()を呼び出してモデルを作る
		logits = inference(images_placeholder, keep_prob)
		# loss()を呼び出して損失を計算
		loss_value = loss(logits, labels_placeholder)
		# training()を呼び出して訓練
		train_op = training(loss_value, FLAGS.learning_rate)
		# 精度の計算
		acc = accuracy(logits, labels_placeholder)

		# 保存の準備
		saver = tf.train.Saver()
		# Sessionの作成
		sess = tf.Session()
		# 変数の初期化
		sess.run(tf.initialize_all_variables())
		# TensorBoardで表示する値の設定
		summary_op = tf.merge_all_summaries()
		summary_writer = tf.train.SummaryWriter(FLAGS.train_dir, sess.graph_def)

		# 訓練の実行
		for step in range(FLAGS.max_steps):
			for i in range(len(train_image)/FLAGS.batch_size):
				# batch_size分の画像に対して訓練の実行
				batch = FLAGS.batch_size*i
				# feed_dictでplaceholderに入れるデータを指定する
				sess.run(train_op, feed_dict={
				  images_placeholder: np.array(map(np.array,train_image[batch:batch+FLAGS.batch_size])),
				  labels_placeholder: np.array(map(np.array,train_label[batch:batch+FLAGS.batch_size])),
				  keep_prob: 0.5})

			# 1 step終わるたびに精度を計算する
			train_accuracy = sess.run(acc, feed_dict={
				images_placeholder: train_image,
				labels_placeholder: train_label,
				keep_prob: 1.0})
			print "step %d, training accuracy %g"%(step, train_accuracy)

			# 1 step終わるたびにTensorBoardに表示する値を追加する
			summary_str = sess.run(summary_op, feed_dict={
				images_placeholder: train_image,
				labels_placeholder: train_label,
				keep_prob:1.0})
			summary_writer.add_summary(summary_str, step)

	# 訓練が終了したらテストデータに対する精度を表示
	print "test accuracy %g"%sess.run(acc, feed_dict={
		images_placeholder: test_image,
		labels_placeholder: test_label,
		keep_prob: 1.0})
	# 最終的なモデルを保存
	save_path = saver.save(sess, "model.ckpt")
